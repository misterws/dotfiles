# dotfiles

macOS personal dotfiles, bootstrapped using [Dotbot](https://github.com/anishathalye/dotbot) & using [Keybase](https://keybase.io) as a private store. Designed to get a new laptop up and running with minimal config, by running:

```
git clone https://gitlab.com/misterws/dotfiles.git && cd dotfiles && ./install private
```